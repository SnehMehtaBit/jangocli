jango v0.0.1
    by Sneh Mehta
----------------------
Last Session Notes:
	-- The setTimeout was able to act mimic awaiting a functions completion before going forward.
		- Might be able to mess with timing?
		- Maybe see what the actual await syntax is and use it with .then()

		- Git Ignore the prettierc.json and notes.txt and .env

		- Start cleaning up old code.
			- Change the tmData logic to just clear it out at that step

		- Thinking about cleaning up all the code and split it up into different js files similar to childProcesses.js

Code 1:1
-------
await function()

get rid of password and env such

move to CTTOOlS and delete local and clone new repo you know the rest
Testing Treatment(s)
--------------------
    -- 62261-2021-78831d_br

jango v0.0.2
-------------
    -- Feature Goals
        - Integrate Chalk for styled console
        - Another cli command to see the entire config file
        - CLI feedback and conversation

        - If using stdin and stdout is easy enough to implement/the same as 
            promptSync npm package - depracate it and replace.

Sample Output from Endpoint
---------------------------
{
  slug: '62261-2021-78831d_br',
  id: 10551,
  name: '62261-2021-78831D_BR',
  hierarchyId: 'f719ecf6c00c59f67eaf',
  scmId: 'git',
  state: 'AVAILABLE',
  statusMessage: 'Available',
  forkable: true,
  project: {
    key: 'CTT',
    id: 821,
    name: 'Creative Tech - Treatments',
    public: false,
    type: 'NORMAL',
    links: { self: [Array] }
  },
  public: false,
  links: { clone: [ [Object], [Object] ], self: [ [Object] ] }
}

Old Commented Code
------------------
    const changeDirectory = () => {
		let directory = spawn('cd', ['..']);
		directory.stdout.on('data', data => {
			console.log('this is data', data.toString());
			let response = data.toString();
		});
		directory.stderr.on('data', data => {
			console.log('this is standard error');
			console.error(data.toString());
		});
		directory.on('exit', code => {
			console.log(`Child exited with code ${code}`);
			if (code === 0) {
				goIntoDocumentsDirectory();
			}
		});
	};

    const goIntoDocumentsDirectory = () => {
		let documentsDirectory = spawn('cd', ['documents']);
		documentsDirectory.stdout.on('data', data => {
			console.log('this is data', data.toString());
			let response = data.toString();
		});
		documentsDirectory.stderr.on('data', data => {
			console.log('this is standard error');
			console.error(data.toString());
		});
		documentsDirectory.on('exit', code => {
			console.log(`Child exited with code ${code}`);
			if (code === 0) {
				gitClone();
			}
		});
	};