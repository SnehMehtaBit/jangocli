const welcome = require('cli-welcome');
const pkg = require('./../package.json');
const unhandled = require('cli-handle-unhandled');

module.exports = ({ clear = true }) => {
	unhandled();
	welcome({
		title: `jango`,
		tagLine: `by Sneh Mehta`,
		description: pkg.description,
		version: pkg.version,
		bgColor: '#7AD7FO',
		color: '#000000',
		bold: true,
		clear
	});
};
