#!/usr/bin/env node

/**
 * jango
 * A CLI tool to help improve the workflow process of cloning a previous treatment for a new client creative.
 *
 * @author Sneh Mehta <https://www.snehmehta.co>
 */

// -----------------------------------------
const init = require('./utils/init');
const cli = require('./utils/cli');
const log = require('./utils/log');
const prompt = require('prompt-sync')();
const fs = require('fs');

const { default: axios } = require('axios');

const path = require('path');
const envfilePath = path.join(__dirname, './.env');

const dotenv = require('dotenv');
dotenv.config({ path: `${envfilePath}` });

const { chdir, cwd } = require('process');
const { exec, spawn, execFile } = require('child_process');
const readline = require('readline');
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

const input = cli.input;
const flags = cli.flags;
const { clear, debug } = flags;
// -----------------------------------------
(async () => {
	init({ clear });
	input.includes(`help`) && cli.showHelp(0);

	debug && log(flags);

	// Node process.chdir => https://nodejs.org/api/process.html#processchdirdirectory
	// Had to use chdir instead of cd due to: => https://superuser.com/questions/1459436/usr-bin-cd-is-not-working-in-mac-terminal
	const changeDirectory = url => {
		console.log(`Starting directory: ${cwd()}`);
		try {
			chdir('/Users/snemehta/Documents');
			console.log(`New directory: ${cwd()}`);
			gitClone(url);
		} catch (err) {
			console.error(`chdir: ${err}`);
		}
	};

	// Node child process: => https://nodejs.org/api/child_process.html#child-process
	const gitClone = url => {
		let args = ['clone', url];
		let clone = spawn('git', args);
		clone.stdout.on('data', data => {
			console.log('this is data', data.toString());
			let response = data.toString();
		});
		clone.stderr.on('data', data => {
			console.error(data.toString());
		});
		clone.on('exit', code => {
			// console.log(`Child exited with code ${code}`);
			if (code === 0) {
				console.log('Succesfully cloned the repository');
			}
		});
	};

	//  User entry point: Treatment ID prompt
	const originalTreatmentToClone = prompt(
		'Which treatment would you like to clone? '
	);

	// Bitbucket CNVR Repo that contains all treatment repos (Bitbucket API endpoint)
	let treatmentPathUrl = `https://git.cnvrmedia.net/projects/CTT/repos/`;

	// Axios get method function to retrieve and authenticate the repo from our CNVR repository on Bitbucket
	function getRepo(url, treatmentId) {
		return (
			axios({
				method: 'get',
				url: `${url}${treatmentId}`,
				auth: {
					username: process.env.STASH_USER,
					password: process.env.STASH_PW
				}
			})
			// MDN .then docs: => https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/then
			.then(res => {
				// res.data.links.clone[0].href is the ssh link for cloning repos
				let treatmentUrl = res.data.links.clone[0].href;
				changeDirectory(treatmentUrl);
				
				setTimeout(() => {
					// Helper function that will read a JSON file and parse it for us
				function jsonReader(filePath, cb) {
					fs.readFile(filePath, 'utf-8', (err, fileData) => {
						if (err) {
							return cb && cb(err);
						}
						try {
							const object = JSON.parse(fileData);
							return cb && cb(null, object);
						} catch (err) {
							return cb && cb(err);
						}
					});
				}

				let localTreatmentPath = `/Users/snemehta/Documents/${originalTreatmentToClone}`;
				fs.readdir(localTreatmentPath, function (err, files) {
					if (err) {
						console.log(err);
						return;
					}
				});

				jsonReader(
					`${localTreatmentPath}/config.json`,
					(err, data) => {
						if (err) {
							console.log(err);
						} else {
							let originalTreatmentDetails = [
								data.companyId,
								data.companyName,
								data.subdirectory
							];
							let oldTmData = {
								parentId: data.tmData.parentId,
								versionId: data.tmData.versionId,
								parentCreativeId:
									data.tmData.parentCreativeId
							};
							console.log('\n');
							console.log(originalTreatmentDetails);

							const cloneCompanyId = prompt(
								'Please enter the companyID for the new treatment: '
							);

							const cloneCompanyName = prompt(
								'Please enter the companyName for the new treatment: '
							);

							const cloneSubdirectory = prompt(
								'Please enter the subdirectory for the new treatment: '
							);

							let cloneTreatmentDetails = [
								cloneCompanyId,
								cloneCompanyName,
								cloneSubdirectory
							];

							// Update a JSON File
							// Read then write then update
							jsonReader(
								`${treatmentPath}/config.json`,
								(err, data) => {
									if (err) {
										console.log(err);
									} else {
										data.companyId = cloneCompanyId;
										data.companyName = cloneCompanyName;
										data.subdirectory =
											cloneSubdirectory;
										fs.writeFile(
											`${treatmentPath}/config.json`,
											JSON.stringify(data, null, 2),
											err => {
												if (err) {
													console.log(err);
												}
											}
										);
									}
								}
							);

							console.log('\n');
							console.log(
								'The original treatment details were: '
							);
							console.log(originalTreatmentDetails);
							console.log('\n');
							console.log(
								'The new cloned treatment details are: '
							);
							console.log(cloneTreatmentDetails);
							console.log('\n');

							// 		// Add in some sort of prompt to make the user continue (Next?)
							// 		// (y) or (n) then clear the console and start with the oldTmData

							console.log(oldTmData);

							const cloneParentId = prompt(
								'Please enter the new parentId assigned to this new creative: '
							);

							const cloneVersionId = prompt(
								'Please enter the new versionId assigned to this new creative: '
							);

							const cloneParentCreativeId = prompt(
								'Please enter the new parentCreativeId assigned to this new creative: '
							);

							jsonReader(
								`${treatmentPath}/config.json`,
								(err, data) => {
									if (err) {
										console.log(err);
									} else {
										data.tmData.parentId =
											cloneParentId;
										data.tmData.versionId =
											cloneVersionId;
										data.tmData.parentCreativeId =
											cloneParentCreativeId;
										fs.writeFile(
											`${treatmentPath}/config.json`,
											JSON.stringify(data, null, 2),
											err => {
												if (err) {
													console.log(err);
												}
											}
										);
									}
								}
							);

							let newTmData = {
								parentId: cloneParentId,
								versionId: cloneVersionId,
								parentCreativeId: cloneParentCreativeId
							};

							console.log('\n');
							console.log(
								'The original tmData details were: '
							);
							console.log(oldTmData);
							console.log('\n');
							console.log(
								'The new cloned tmData details are: '
							);
							console.log(newTmData);

							console.log('\n');
							const cloneCompleteMessage = prompt(
								'Clone Complete! Time to upload to Treatment Manager. Press Enter to End... '
							);
						}
					}
				);
				}, 5000);
			})
			.catch(err => console.log(err))
		);
	}
	// Passing in the treatmentPath template url and the treatmentID passed from the prompt
	getRepo(treatmentPathUrl, originalTreatmentToClone);
})();
