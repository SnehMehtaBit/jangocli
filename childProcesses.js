const { exec, spawn, execFile } = require('child_process');
const readline = require('readline');
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});
let createProdFiles = spawn('rcli', ['build', '-p']);
createProdFiles.stdout.on('data', data => {
	console.log('this is data', data.toString());
	let response = data.toString();
	if (response.includes('Serving files from:')) {
		createProdFiles.kill();
	}
});
createProdFiles.stderr.on('data', data => {
	console.log('this is standard error');
	console.error(data.toString()); 
});
createProdFiles.on('exit', code => {
	console.log(`Child exited with code ${code}`);
	console.log('run git process');
	gitStatus();
});
const gitStatus = () => {
	let status = spawn('git', ['status']);
	status.stdout.on('data', data => {
		console.log('this is data', data.toString());
		let response = data.toString();
	});
	status.stderr.on('data', data => {
		console.log('this is standard error');
		console.error(data.toString());
	});
	status.on('exit', code => {
		console.log(`Child exited with code ${code}`);
		if (code === 0) {
			gitAdd();
		}
	});
};
const gitAdd = () => {
	let add = spawn('git', ['add', '.']);
	add.stdout.on('data', data => {
		console.log('git ADD FILE ');
		console.log('this is data', data.toString());
		let response = data.toString();
	});
	add.stderr.on('data', data => {
		console.log('this is standard error');
		console.error(data.toString());
	});
	add.on('exit', code => {
		console.log(`Child exited with code ${code}`);
		if (code === 0) {
			console.log('new files added to git');
			// gitCommit()
			commitPrompt();
		}
	});
};
const commitPrompt = () => {
	let msg;
	rl.question('Please Enter Commit Message: ', input => {
		msg = input;
		rl.close();
	});
	rl.on('close', () => {
		gitCommit(msg);
		//process.exit(0)
	});
};
const gitCommit = msg => {
	console.log('THIS IS MESSAGE:', msg);
	// if(msg === ''){
	//   let date = new Date()
	//   let dateFormat = date.toDateString()
	//   let timeFormat = date.toTimeString()
	//   msg = 'Most recent upload to treatment manager ' + dateFormat + ` ${timeFormat}`
	//   console.log('should be a date', msg )
	// }
	let args = ['commit', '-m', msg];
	let commit = spawn('git', args);
	commit.stdout.on('data', data => {
		console.log('this is data', data.toString());
	});
	commit.stderr.on('data', data => {
		console.log('commit error');
		console.error(data.toString());
	});
	commit.on('exit', code => {
		console.log(`Child exited with code ${code}`);
		if (code === 0) {
			console.log('FILES COMMITTED');
			getBranch();
		}
	});
};
const getBranch = () => {
	let branch = spawn('git', ['branch', '--show-current']);
	let branchName;
	branch.stdout.on('data', data => {
		console.log('git ADD FILE ');
		console.log('BRANCH this is data', data.toString());
		branchName = data.toString().slice(0, -1);
	});
	branch.stderr.on('data', data => {
		console.log('this is standard error');
		console.error(data.toString());
	});
	branch.on('exit', code => {
		console.log(`BRANCH Child exited with code ${code}`);
		if (code === 0) {
			console.log('branch name retrieved');
			gitPush(branchName);
		}
	});
};
const gitPush = branchName => {
	let args = ['push', 'origin', branchName];
	let push = spawn('git', args);
	push.stdout.on('data', data => {
		console.log('git PUSH FILE ');
		console.log('this is data', data.toString());
		let response = data.toString();
	});
	push.stderr.on('data', data => {
		// console.log('this is PUSH error')
		console.error(data.toString());
	});
	push.on('exit', code => {
		console.log(`Child exited with code ${code}`);
		if (code === 0) {
			console.log('new files pushed to bitbucket');
			uploadToTreatmentManager();
		}
	});
};
const uploadToTreatmentManager = () => {
	spawn('rcli', ['tm', '-u'], { stdio: 'inherit', stdin: 'inherit' });
};
